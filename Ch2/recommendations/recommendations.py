import metrics


# Returns the best matches for a subject from a preferences dictionary
def top_matches(prefs, subject, n=5, similarity=metrics.pearson_similarity):
    scores = [(similarity(prefs, subject, other), other) for other in prefs if other!=subject]
    scores.sort()
    scores.reverse()
    return scores[0:n]


# Get recommendations for a subject by using a weighted
# average of the preferences of all other subjects
def get_recommendations(prefs, subject, similarity=metrics.pearson_similarity):
    totals = {}
    sim_sums = {}

    for other in prefs:
        # Don't self-compare
        if other==subject: continue
        sim = similarity(prefs, subject, other)

        for item in prefs[other]:
            # Only recommend items the subject has not rated
            if item not in prefs[subject] or prefs[subject][item]==0:
                totals.setdefault(item, 0)
                totals[item] += sim*prefs[other][item]
                sim_sums.setdefault(item, 0)
                sim_sums[item] += sim

    # Create and return the recommendations
    recs = [(total/sim_sums[item], item) for item,total in totals.items()]
    recs.sort()
    recs.reverse()
    return recs
