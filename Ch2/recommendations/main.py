﻿import datasets
import metrics
import recommendations


def main():
    print("Lisa Rose's rating for Lady in the Water")
    print(datasets.critics['Lisa Rose']['Lady in the Water'])

    print("Toby's ratings")
    print(datasets.critics['Toby'])

    print("Manhattan similarity between Lisa Rose and Gene Seymour")
    print(metrics.manhattan_similarity(datasets.critics, 'Lisa Rose', 'Gene Seymour'))

    print("Euclidian similarity between Lisa Rose and Gene Seymour")
    print(metrics.euclidian_similarity(datasets.critics, 'Lisa Rose', 'Gene Seymour'))

    print("Pearson similarity between Lisa Rose and Gene Seymour")
    print(metrics.pearson_similarity(datasets.critics, 'Lisa Rose', 'Gene Seymour'))

    print("Jaccard similarity between Lisa Rose and Gene Seymour")
    print(metrics.jaccard_similarity(datasets.critics, 'Lisa Rose', 'Gene Seymour'))

    print("Tanimoto similarity between Lisa Rose and Gene Seymour")
    print(metrics.tanimoto_similarity(datasets.critics, 'Lisa Rose', 'Gene Seymour'))

    print("\nTOP 3 CRITICS MOST SIMILAR TO TOBY")
    print("Manhattan")
    print(recommendations.top_matches(datasets.critics, 'Toby', 3, similarity=metrics.manhattan_similarity))
    print("Euclidian")
    print(recommendations.top_matches(datasets.critics, 'Toby', 3, similarity=metrics.euclidian_similarity))
    print("Pearson")
    print(recommendations.top_matches(datasets.critics, 'Toby', 3, similarity=metrics.pearson_similarity))
    print("Jaccard")
    print(recommendations.top_matches(datasets.critics, 'Toby', 3, similarity=metrics.jaccard_similarity))
    print("Tanimoto")
    print(recommendations.top_matches(datasets.critics, 'Toby', 3, similarity=metrics.tanimoto_similarity))

    print("\nRECOMMENDATIONS FOR TOBY")
    print("Manhattan")
    print(recommendations.get_recommendations(datasets.critics, 'Toby', similarity=metrics.manhattan_similarity))
    print("Euclidian")
    print(recommendations.get_recommendations(datasets.critics, 'Toby', similarity=metrics.euclidian_similarity))
    print("Pearson")
    print(recommendations.get_recommendations(datasets.critics, 'Toby', similarity=metrics.pearson_similarity))
    print("Jaccard")
    print(recommendations.get_recommendations(datasets.critics, 'Toby', similarity=metrics.jaccard_similarity))
    print("Tanimoto")
    print(recommendations.get_recommendations(datasets.critics, 'Toby', similarity=metrics.tanimoto_similarity))
    movies = datasets.get_movies_dict()

    print("\nMOVIES MOST SIMILARLY RATED TO SUPERMAN RETURNS")
    print("Pearson")
    print(recommendations.top_matches(movies, 'Superman Returns'))
    print("Tanimoto")
    print(recommendations.top_matches(movies, 'Superman Returns', similarity=metrics.tanimoto_similarity))

    print("\nCRITICS MOST LIKELY TO RATE JUST MY LUCK FAVOURABLY")
    print("Pearson")
    print(recommendations.get_recommendations(movies, 'Just My Luck'))
    print("Tanimoto")
    print(recommendations.get_recommendations(movies, 'Just My Luck', similarity=metrics.tanimoto_similarity))


if __name__ == "__main__":
    main()
