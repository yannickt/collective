from math import sqrt


# Returns a similarity score for two subjects based on
# the Manhattan distance between their common preferences
def manhattan_similarity(prefs, subject1, subject2):
    # Get the list of mutually rated items
    shared_items = {}
    for item in prefs[subject1]:
        if item in prefs[subject2]:
            shared_items[item] = 1
    # Stop if they have no preferences in common
    if len(shared_items)==0: return 0
    # Calculate the similarity score
    sum_of_quares = sum([abs(prefs[subject1][item]-prefs[subject2][item]) for item in shared_items])
    return 1/(1+sqrt(sum_of_quares))


# Returns a similarity score for two subjects based on
# the Euclidian distance between their common preferences
def euclidian_similarity(prefs, subject1, subject2):
    # Get the list of mutually rated items
    shared_items = {}
    for item in prefs[subject1]:
        if item in prefs[subject2]:
            shared_items[item] = 1
    # Stop if they have no preferences in common
    if len(shared_items)==0: return 0
    # Calculate the similarity score
    sum_of_quares = sum([pow(prefs[subject1][item]-prefs[subject2][item], 2)
                        for item in shared_items])
    return 1/(1+sqrt(sum_of_quares))


# Returns a similarity score for two subjects based
# on the Pearson correlation of their common preferences
def pearson_similarity(prefs, subject1, subject2):
    # Get the list of mutually rated items
    shared_items = {}
    for item in prefs[subject1]:
        if item in prefs[subject2]:
            shared_items[item] = 1
    num_shared = len(shared_items)
    # Stop if they have no preferences in common
    if num_shared==0: return -1
    # Add up all the preferences
    sum1 = sum(prefs[subject1][item] for item in shared_items)
    sum2 = sum(prefs[subject2][item] for item in shared_items)
    # Sum up the squares
    sum1_sq = sum(pow(prefs[subject1][item], 2) for item in shared_items)
    sum2_sq = sum(pow(prefs[subject2][item], 2) for item in shared_items)
    # Sum up the products
    sum_prod = sum(prefs[subject1][item]*prefs[subject2][item] for item in shared_items)
    # Calculate Pearson score
    num = sum_prod - (sum1*sum2/num_shared)
    den = sqrt((sum1_sq - pow(sum1,2)/num_shared) * (sum2_sq - pow(sum2,2)/num_shared))
    return 0 if den==0 else num/den


# Returns the Jaccard similarity between two subjects
def jaccard_similarity(prefs, subject1, subject2):
    len_intersect = 0
    len_union = 0
    for item in prefs[subject1]:
        if item in prefs[subject2]:
            len_intersect += 1
    len_union = len(prefs[subject1]) + len(prefs[subject2]) - len_intersect
    return 1 if len_union==0 else len_intersect/len_union


# Returns the Tanimoto similarity between two subjects
def tanimoto_similarity(prefs, subject1, subject2):
    dot_product = 0
    for item in prefs[subject1]:
        if item in prefs[subject2]:
            dot_product += prefs[subject1][item]*prefs[subject2][item]
    num = dot_product
    sum1_sq = sum(pow(prefs[subject1][item], 2) for item in prefs[subject1])
    sum2_sq = sum(pow(prefs[subject2][item], 2) for item in prefs[subject2])
    den = sum1_sq + sum2_sq - dot_product
    return 1 if den==0 else num/den
