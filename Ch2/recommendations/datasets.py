import os
import shutil
import urllib.request
import zipfile

# A dictionary of movie critics and their ratings of a small set of movies
critics = {'Lisa Rose': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.5,
                         'Just My Luck': 3.0, 'Superman Returns': 3.5,
                         'You, Me and Dupree': 2.5, 'The Night Listener': 3.0},
           'Gene Seymour': {'Lady in the Water': 3.0, 'Snakes on a Plane': 3.5,
                            'Just My Luck': 1.5, 'Superman Returns': 5.0,
                            'The Night Listener': 3.0, 'You, Me and Dupree': 3.5}, 
           'Michael Phillips': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.0,
                                'Superman Returns': 3.5, 'The Night Listener': 4.0},
           'Claudia Puig': {'Snakes on a Plane': 3.5, 'Just My Luck': 3.0,
                            'The Night Listener': 4.5, 'Superman Returns': 4.0, 
                            'You, Me and Dupree': 2.5},
           'Mick LaSalle': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0, 
                            'Just My Luck': 2.0, 'Superman Returns': 3.0, 'The Night Listener': 3.0,
                            'You, Me and Dupree': 2.0}, 
           'Jack Matthews': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0,
                             'The Night Listener': 3.0, 'Superman Returns': 5.0, 'You, Me and Dupree': 3.5},
           'Toby': {'Snakes on a Plane':4.5,'You, Me and Dupree':1.0,'Superman Returns':4.0}
          }


def get_movies_dict():
    result = {}
    for person in critics:
        for movie in critics[person]:
            result.setdefault(movie, {})
            result[movie][person] = critics[person][movie]
    return result


def cleanup():
    shutil.rmtree("./datasets")


def get_delicious():
    if not os.path.exists("./datasets"):
        os.mkdir("./datasets")
    urllib.request.urlretrieve("http://files.grouplens.org/datasets/hetrec2011/hetrec2011-delicious-2k.zip", "./datasets/delicious.zip")
    with zipfile.ZipFile("./datasets/delicious.zip") as arch:
        arch.extractall("./datasets/delicious/")
    os.remove("./datasets/delicious.zip")


def get_movielens():
    if not os.path.exists("./datasets"):
        os.mkdir("./datasets")
    urllib.request.urlretrieve("http://files.grouplens.org/datasets/movielens/ml-100k.zip", "./datasets/movielens.zip")
    with zipfile.ZipFile("./datasets/movielens.zip") as arch:
        arch.extractall("./datasets/")
    os.remove("./datasets/movielens.zip")